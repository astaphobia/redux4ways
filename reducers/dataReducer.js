import {
FETCHING_DATA,
	FETCHING_DATA_SUCCESS,
	FETCHING_DATA_FAILURE,
} from './constans';

const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
};

export default function dataReducer(state = initialState, action){
  switch(action.type){
    case FETCHING_DATA:
      return {
        ...state,
        dat: [],
        isFetching: true,
      };
    case FETCHING_DATA_SUCCESS:
      return {
        ...state,
        isFeteching: false,
        data: action.data,
      };
    case FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    defualt: 
      return state;
  }
}
